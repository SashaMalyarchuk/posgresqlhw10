CREATE DATABASE hw10_address



CREATE TABLE IF NOT EXISTS address(
    id serial PRIMARY KEY,
    Country varchar(20) NOT NULL,
    City varchar(30) NOT NULL,
    Street varchar(35) NOT NULL,
    ZipCode varchar(5) NOT NULL,
  UNNIQU(Country, City, Street, ZIPCode)
);


CREATE TABLE IF NOT EXISTS(
    id serial PRIMARY KEY,
    First_name varchar(30) NOT NULL,
    Last_name varchar(30) NOT NULL,
    age int CHECK(age > 0) NOT NULL
);

CREATE TABLE IF NOT EXISTS location (
    id serial PRIMARY KEY,
    name varchar(30) NOT NULL UNNIQU,
    score int, 
    location_address int REFERENCES address(id)
)

CREATE TABLE IF NOT EXISTS user_location (
    id serial PRIMARY KEY,
    user_id int REFERENCES user(id),
    address_id int REFERENCES address(id)
);